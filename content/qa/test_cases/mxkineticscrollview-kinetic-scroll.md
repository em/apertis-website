+++
date = "2017-10-23"
weight = 100

title = "mxkineticscrollview-kinetic-scroll"

aliases = [
    "/old-wiki/QA/Test_Cases/mxkineticscrollview-kinetic-scroll"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/

+++
date = "2018-06-25"
weight = 100

title = "boot-performance-automated"

aliases = [
    "/old-wiki/QA/Test_Cases/boot-performance-automated"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/

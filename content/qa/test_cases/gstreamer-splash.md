+++
date = "2018-04-18"
weight = 100

title = "gstreamer-splash"

aliases = [
    "/old-wiki/QA/Test_Cases/gstreamer-splash"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
